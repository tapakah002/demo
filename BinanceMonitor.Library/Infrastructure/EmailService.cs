﻿using BinanceMonitor.Library.Configs;
using BinanceMonitor.Library.Domain;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System.Net;
using System.Net.Mail;

namespace BinanceMonitor.Library.Infrastructure
{
    public class EmailService : IEmailService
    {
        private readonly EmailServiceConfig _config;
        private ILogger _logger;

        public EmailService(ILoggerFactory loggerFactory, IOptions<EmailServiceConfig> config)
        {
            _config = config.Value;
            _logger = loggerFactory.CreateLogger<EmailService>();
        }

        public void Send(EmailMessage message)
        {
            var smtp = new SmtpClient
            {
                Host = _config.Host,
                Port = _config.Port,
                EnableSsl = true,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                UseDefaultCredentials = false,
                Credentials = new NetworkCredential(_config.From, _config.Password)
            };
            using (var mailMessage = new MailMessage(_config.From, _config.To)
            {
                Subject = message.Subject,
                Body = message.Body
            })
            {
                smtp.Send(mailMessage);
            }
        }
    }
}