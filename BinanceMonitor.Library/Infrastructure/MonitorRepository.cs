﻿using AutoMapper;
using BinanceMonitor.Context;
using BinanceMonitor.Context.Entities;
using BinanceMonitor.Library.Configs;
using BinanceMonitor.Library.Domain.Repository;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BinanceMonitor.Library.Infrastructure
{
    public class MonitorRepository : IMonitorRepository
    {
        private readonly BiMonContextFactory _contextFactory;
        private readonly MapperConfiguration _mapperConfiguration = AutoMapperConfigs.GetMonitorRepositoryConfig();

        public MonitorRepository(BiMonContextFactory dbContextFactory)
        {
            _contextFactory = dbContextFactory;
        }

        public IEnumerable<string> GetStockSymbols()
        {
            using (var context = _contextFactory.Create())
            {
                var mapper = new Mapper(_mapperConfiguration);
                return context.StockSymbols.AsNoTracking().Select(x => x.Value).ToList();
            }
        }

        public IEnumerable<string> GetTradeSymbols()
        {
            using (var context = _contextFactory.Create())
            {
                var mapper = new Mapper(_mapperConfiguration);
                return context.TradeSymbols.AsNoTracking().Select(x => x.Value).ToList();
            }
        }

        public NotificationsList GetNotifications()
        {
            using (var context = _contextFactory.Create())
            {
                var mapper = new Mapper(_mapperConfiguration);
                var result = mapper.Map<IEnumerable<Notifications>, List<NotificationRecord>>(context.Notifications.AsNoTracking().ToList());
                return new NotificationsList(result);
            }
        }

        public TradingHistoryList GetTradingHistory(DateTime? from = null, DateTime? to = null)
        {
            using (var context = _contextFactory.Create())
            {
                var expression = context.TradingHistory.AsNoTracking();
                expression = from.HasValue ? expression.Where(x => x.CreateDate >= from.Value) : expression;
                expression = to.HasValue ? expression.Where(x => x.CreateDate <= to.Value) : expression;

                var mapper = new Mapper(_mapperConfiguration);
                var result = mapper.Map<IEnumerable<TradingHistory>, List<TradingHistoryRecord>>(expression.ToList());

                return new TradingHistoryList(result);
            }
        }

        public void RemoveNotification(NotificationRecord notificationRecord)
        {
            var mapper = new Mapper(_mapperConfiguration);
            var domainNoty = mapper.Map<NotificationRecord, Notifications>(notificationRecord);

            using (var context = _contextFactory.Create())
            {
                context.Remove(context.Notifications.Single(x => x.Id == notificationRecord.Id));
                context.SaveChanges();
            }
        }

        public void SaveNotification(NotificationRecord rec)
        {
            var mapper = new Mapper(_mapperConfiguration);
            var domainNoty = mapper.Map<NotificationRecord, Notifications>(rec);

            //TODO валидация

            using (var context = _contextFactory.Create())
            {
                if (domainNoty.Id == 0)
                    context.Add(domainNoty);
                else
                {
                    if (context.Notifications.Count(x => x.Id == domainNoty.Id) == 0)
                        throw new ArgumentException($"Record doesn't exist: Id {domainNoty.Id}");

                    context.Attach(domainNoty);
                    context.Entry(domainNoty).State = EntityState.Modified;
                }

                context.SaveChanges();

                rec.Id = domainNoty.Id;
            }
        }

        public void AddTradeSymbols(string symbol)
        {
            using (var context = _contextFactory.Create())
            {
                if (context.TradeSymbols.Count(x => x.Value == symbol) != 0)
                    return;

                context.TradeSymbols.Add(new TradeSymbols { Value = symbol });
                context.SaveChanges();
            }
        }

        public void RemoveTradeSymbols(string symbol)
        {
            using (var context = _contextFactory.Create())
            {
                if (context.TradeSymbols.Count(x => x.Value == symbol) == 0)
                    return;

                context.TradeSymbols.Remove(context.TradeSymbols.AsNoTracking().Single(x => x.Value == symbol));
                context.SaveChanges();
            }
        }
    }
}