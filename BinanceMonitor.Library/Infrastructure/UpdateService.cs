﻿using AutoMapper;
using Binance.Net;
using Binance.Net.Objects.Spot.SpotData;
using BinanceMonitor.Context;
using BinanceMonitor.Context.Entities;
using BinanceMonitor.Library.Configs;
using BinanceMonitor.Library.Domain;
using Microsoft.Extensions.Logging;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BinanceMonitor.Library.Infrastructure
{
    public class UpdateService : IUpdateService
    {
        private readonly ILogger<UpdateService> _logger;
        private readonly BinanceClient _binanceClient;
        private readonly BiMonContextFactory _contextFactory;
        private static readonly MapperConfiguration _mapperConfiguration = AutoMapperConfigs.GetHistoryUpdateConfig();

        public UpdateService(ILoggerFactory loggerFactory,
                             BinanceClient binanceClient,
                             BiMonContextFactory contextFactory)
        {
            _logger = loggerFactory.CreateLogger<UpdateService>();
            _binanceClient = binanceClient;
            _contextFactory = contextFactory;
        }

        public void UpdateTradingHistory()
        {
            _logger.LogInformation("Start update history");

            List<string> symbols = GetAvaliableSymbols();
            ConcurrentBag<Task> tasks = new ConcurrentBag<Task>();
            Parallel.ForEach(symbols, symbol => tasks.Add(UpdateSymbol(symbol)));
            Task.WaitAll(tasks.ToArray());

            _logger.LogInformation("End update history");
        }

        private async Task UpdateSymbol(string symbol)
        {
            var result = await _binanceClient.Spot.Order.GetMyTradesAsync(symbol);
            var mapper = new Mapper(_mapperConfiguration);

            var mappedHistory = mapper.Map<IEnumerable<BinanceTrade>, IEnumerable<TradingHistory>>(result.Data);

            _logger.LogInformation("Found {0} history records for symbol {1}", mappedHistory.Count(), symbol);

            using (var context = _contextFactory.Create(true))
            {
                var memoryHistoryRepresent = mappedHistory.ToList();
                var joinedTables = memoryHistoryRepresent.GroupJoin(context.TradingHistory,
                                                               l => l.OrderId,
                                                               r => r.OrderId,
                                                               (l, r) => new { Left = l, Right = r.DefaultIfEmpty() })
                                                         .SelectMany(x => x.Right.Select(y => new { x, y })).Where(x => x.y == null);

                var count = joinedTables.ToList().Select(x => x.x.Left);

                await context.TradingHistory.AddRangeAsync(count);
                await context.SaveChangesAsync();
            }
        }

        private List<string> GetAvaliableSymbols()
        {
            using (var context = _contextFactory.Create())
            {
                return context.TradeSymbols.Select(x => x.Value).ToList();
            }
        }
    }
}