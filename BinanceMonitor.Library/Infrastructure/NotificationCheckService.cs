﻿using Binance.Net;
using BinanceMonitor.Context;
using BinanceMonitor.Context.Entities;
using BinanceMonitor.Library.Domain;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BinanceMonitor.Library.Infrastructure
{
    public class NotificationCheckService : INotificationCheckService
    {
        private readonly ILogger _logger;
        private readonly BinanceClient _binanceClient;
        private readonly IEmailService _emailService;
        private readonly BiMonContextFactory _contextFactory;

        public NotificationCheckService(ILoggerFactory loggerFactory,
                                        BinanceClient binanceClient,
                                        IEmailService emailService,
                                        BiMonContextFactory contextFactory)
        {
            _logger = loggerFactory.CreateLogger<NotificationCheckService>();
            _binanceClient = binanceClient;
            _emailService = emailService;
            _contextFactory = contextFactory;
        }

        public IEnumerable<Notifications> CheckNotifyRecords()
        {
            List<Notifications> result = new List<Notifications>();

            IEnumerable<IGrouping<string, Notifications>> notyRecords;

            using (var context = _contextFactory.Create())
            {
                notyRecords = context.Notifications.AsNoTracking()
                                                   .Where(x => !x.IsInvoked)
                                                   .ToList()
                                                   .GroupBy(x => x.Symbol);
            }

            ConcurrentBag<Task<IEnumerable<Notifications>>> tasks = new ConcurrentBag<Task<IEnumerable<Notifications>>>();
            Parallel.ForEach(notyRecords, x => tasks.Add(CheckSymbol(x)));
            Task.WaitAll(tasks.ToArray());

            return tasks.SelectMany(x => x.Result);
        }

        public void Notify(IEnumerable<Notifications> notifyNeededList)
        {
            using (var context = _contextFactory.Create())
            {
                foreach (var item in notifyNeededList)
                {
                    _emailService.Send(new EmailMessage
                    {
                        Subject = $"Изменение цены {item.Symbol}",
                        Body = $"Достигнуто пороговое значение символа {item.Symbol}. " +
                               $"Цена покупки: {item.InitialPrice.ToString().Replace(',', '.')}; " +
                               $"Пороговая цена: {item.ThresholdPrice.ToString().Replace(',', '.')}; " +
                               $"Текущая цена {_binanceClient.Spot.Market.Get24HPrice(item.Symbol).Data.LastPrice.ToString().Replace(',', '.')}"
                    });

                    context.Notifications.Single(x => x.Id == item.Id).IsInvoked = true;
                }

                context.SaveChanges();
            }
        }

        private async Task<IEnumerable<Notifications>> CheckSymbol(IGrouping<string, Notifications> groupedNoties)
        {
            List<Notifications> result = new List<Notifications>();

            var avgPrice = (await _binanceClient.Spot.Market.Get24HPriceAsync(groupedNoties.Key)).Data;

            if (avgPrice == null)
                return result;

            foreach (var noty in groupedNoties)
            {
                if (noty.ThresholdPrice < decimal.ToDouble(avgPrice.LastPrice))
                    result.Add(noty);
            }

            return result;
        }
    }
}