﻿namespace BinanceMonitor.Library.Domain
{
    public class EmailMessage
    {
        public string Subject { get; set; }
        public string Body { get; set; }
    }
}