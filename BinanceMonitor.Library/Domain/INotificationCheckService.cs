﻿using BinanceMonitor.Context.Entities;
using System.Collections.Generic;

namespace BinanceMonitor.Library.Domain
{
    public interface INotificationCheckService
    {
        IEnumerable<Notifications> CheckNotifyRecords();
        void Notify(IEnumerable<Notifications> notifyNeededList);
    }
}