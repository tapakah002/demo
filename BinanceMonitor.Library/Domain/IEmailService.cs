﻿namespace BinanceMonitor.Library.Domain
{
    public interface IEmailService
    {
        void Send(EmailMessage message);
    }
}