﻿namespace BinanceMonitor.Library.Domain
{
    public interface IUpdateService
    {
        void UpdateTradingHistory();
    }
}