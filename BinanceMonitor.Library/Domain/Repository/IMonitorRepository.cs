﻿using System;
using System.Collections.Generic;

namespace BinanceMonitor.Library.Domain.Repository
{
    public interface IMonitorRepository
    {
        NotificationsList GetNotifications();
        TradingHistoryList GetTradingHistory(DateTime? from = null, DateTime? to = null);
        void SaveNotification(NotificationRecord rec);
        void RemoveNotification(NotificationRecord notificationRecord);
        IEnumerable<string> GetTradeSymbols();
        void AddTradeSymbols(string symbol);
        void RemoveTradeSymbols(string symbol);
        IEnumerable<string> GetStockSymbols();
    }
}