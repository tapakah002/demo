﻿using BinanceMonitor.Context.Lookup;
using System;

namespace BinanceMonitor.Library.Domain.Repository
{
    public class TradingHistoryRecord
    {
        public int Id { get; set; }

        public string Symbol { get; set; }

        public double Price { get; set; }

        public double Quantity { get; set; }

        public DateTime CreateDate { get; set; }
        public TradeType TradeType { get; set; }
    }
}