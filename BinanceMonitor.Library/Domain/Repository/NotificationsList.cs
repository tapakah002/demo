﻿using System.Collections.Generic;

namespace BinanceMonitor.Library.Domain.Repository
{
    public class NotificationsList : List<NotificationRecord>
    {
        public NotificationsList() : base()
        { }

        public NotificationsList(IEnumerable<NotificationRecord> collection) : base(collection)
        { }
    }
}