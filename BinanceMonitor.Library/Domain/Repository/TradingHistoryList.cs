﻿using System.Collections.Generic;

namespace BinanceMonitor.Library.Domain.Repository
{
    public class TradingHistoryList : List<TradingHistoryRecord>
    {
        public TradingHistoryList() : base()
        { }

        public TradingHistoryList(IEnumerable<TradingHistoryRecord> collection) : base(collection)
        { }
    }
}