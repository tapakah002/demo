﻿using BinanceMonitor.Context.Lookup;
using System;

namespace BinanceMonitor.Library.Domain.Repository
{
    public class NotificationRecord
    {
        public int Id { get; set; }

        public string Symbol { get; set; }

        public decimal? InitialPrice { get; set; }
        public decimal? ThresholdPrice { get; set; }
        public DateTime CreateDate { get; set; }
        public bool IsInvoked { get; set; }
        public string Comment { get; set; }
        public NotifyType NotifyType { get; set; }
    }
}