﻿using AutoMapper;
using Binance.Net.Objects.Spot.SpotData;
using BinanceMonitor.Context.Entities;
using BinanceMonitor.Context.Lookup;
using BinanceMonitor.Library.Domain.Repository;

namespace BinanceMonitor.Library.Configs
{
    public static class AutoMapperConfigs
    {
        public static MapperConfiguration GetHistoryUpdateConfig()
        {
            return new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<BinanceTrade, TradingHistory>()
                .ForMember(x => x.Id, x => x.Ignore())
                .ForMember(x => x.TradeType, p => p.MapFrom(x => x.IsBuyer == true ? TradeType.Buy : TradeType.Sell))
                .ForMember(x => x.CreateDate, p => p.MapFrom(x => x.TradeTime))
                .ReverseMap();
            });
        }

        public static MapperConfiguration GetMonitorRepositoryConfig()
        {
            return new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<NotificationRecord, Notifications>()
                .ReverseMap();

                cfg.CreateMap<TradingHistoryRecord, TradingHistory>()
                .ReverseMap();
            });
        }
    }
}