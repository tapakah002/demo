﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BinanceMonitor.Library.Configs
{
    public class BinanceCredentialsConfig
    {
        public string Key { get; set; }
        public string Secret { get; set; }
    }
}
