using BinanceMonitor.Library.Infrastructure.Context;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Logging.Abstractions;
using Microsoft.Extensions.Options;
using Moq;
using System;
using Xunit;

namespace BinanceMonitor.Library.Infrastructure.Context
{
    public class DbContextFactoryTests
    {
        private MockRepository mockRepository;

        private Mock<IOptions<MonitorContextConfig>> mockOptions;
        //private Mock<ILoggerFactory> mockLoggerFactory;

        public DbContextFactoryTests()
        {
            this.mockRepository = new MockRepository(MockBehavior.Strict);

            this.mockOptions = this.mockRepository.Create<IOptions<MonitorContextConfig>>();
            mockOptions.Setup(x => x.Value).Returns(new MonitorContextConfig { ConnectionString = "Database=dcyodbex;Password=N9odunhvCrFDqYVbEIFcs9jmhOVk_oOu;Username=dcyodbex;Persist Security Info=True;Host=kandula.db.elephantsql.com" });

            //this.mockLoggerFactory = this.mockRepository.Create<ILoggerFactory>();
            //mockLoggerFactory.Setup(x => x.)
        }

        private DbContextFactory CreateFactory()
        {
            return new DbContextFactory(
                this.mockOptions.Object,
                NullLoggerFactory.Instance);
        }

        [Fact]
        public void Create_StateUnderTest_ExpectedBehavior()
        {
            // Arrange
            var factory = this.CreateFactory();
            bool useLogger = false;

            // Act
            var result = factory.Create(
                useLogger);

            // Assert
            Assert.True(false);
            this.mockRepository.VerifyAll();
        }
    }
}
