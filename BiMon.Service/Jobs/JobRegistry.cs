﻿using FluentScheduler;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace BiMon.Service.Jobs
{
    public class JobRegistry : Registry
    {
        public JobRegistry(IServiceProvider sp)
        {
            Schedule(() => sp.CreateScope().ServiceProvider.GetRequiredService<UpdateTradingHistoryJob>()).NonReentrant().ToRunNow().AndEvery(10).Seconds();
            Schedule(() => sp.CreateScope().ServiceProvider.GetRequiredService<CheckNotificationJob>()).NonReentrant().ToRunNow().AndEvery(10).Seconds();
            Schedule(() => sp.CreateScope().ServiceProvider.GetRequiredService<CheckStockSymbolsJob>()).NonReentrant().ToRunNow().AndEvery(1).Days();
        }
    }
}