﻿using BinanceMonitor.Context;
using BinanceMonitor.Library.Domain;
using FluentScheduler;
using Microsoft.Extensions.Logging;

namespace BiMon.Service.Jobs
{
    internal class CheckNotificationJob : IJob
    {
        private readonly BiMonContextFactory _context;
        private readonly INotificationCheckService _notificationCheckService;
        private ILogger _logger;

        public CheckNotificationJob()
        {
        }

        public CheckNotificationJob(ILoggerFactory loggerFactory,
                                    BiMonContextFactory context,
                                    INotificationCheckService notificationCheckService)
        {
            _logger = loggerFactory.CreateLogger<CheckNotificationJob>();
            _context = context;
            _notificationCheckService = notificationCheckService;
        }

        public void Execute()
        {
            _logger.LogInformation("Execute CheckNotificationJob");

            var notifyNeededList = _notificationCheckService.CheckNotifyRecords();
            _notificationCheckService.Notify(notifyNeededList);

            _logger.LogInformation("End CheckNotificationJob");
        }
    }
}