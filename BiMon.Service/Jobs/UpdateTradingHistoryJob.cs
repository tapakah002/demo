﻿using BinanceMonitor.Context;
using BinanceMonitor.Library.Domain;
using FluentScheduler;
using Microsoft.Extensions.Logging;

namespace BiMon.Service.Jobs
{
    public class UpdateTradingHistoryJob : IJob
    {
        private readonly BiMonContextFactory _context;
        private readonly IUpdateService _updateService;
        private ILogger _logger;

        public UpdateTradingHistoryJob()
        {
        }

        public UpdateTradingHistoryJob(ILoggerFactory loggerFactory,
                                       BiMonContextFactory context,
                                       IUpdateService updateService)
        {
            _logger = loggerFactory.CreateLogger<UpdateTradingHistoryJob>();
            _context = context;
            _updateService = updateService;
        }

        public void Execute()
        {
            try
            {
                _logger.LogInformation("Execute UpdateTradingHistoryJob");
                _updateService.UpdateTradingHistory();
                _logger.LogInformation("End UpdateTradingHistoryJob");
            }
            catch (System.Exception ex)
            {
                _logger.LogError(ex.ToString());
                throw;
            }
        }
    }
}