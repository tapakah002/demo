﻿using Binance.Net;
using BinanceMonitor.Context;
using BinanceMonitor.Context.Entities;
using FluentScheduler;
using Microsoft.Extensions.Logging;
using System.Linq;

namespace BiMon.Service.Jobs
{
    internal class CheckStockSymbolsJob : IJob
    {
        private readonly BiMonContextFactory _contextFactory;
        private readonly BinanceClient _binanceClient;
        private ILogger _logger;

        public CheckStockSymbolsJob()
        {
        }

        public CheckStockSymbolsJob(ILoggerFactory loggerFactory,
                                    BiMonContextFactory contextFactory,
                                    BinanceClient binanceClient)
        {
            _logger = loggerFactory.CreateLogger<CheckStockSymbolsJob>();
            _contextFactory = contextFactory;
            _binanceClient = binanceClient;
        }

        public void Execute()
        {
            _logger.LogInformation("Execute CheckStockSymbolsJob");

            var symbols = _binanceClient.Spot.System.GetExchangeInfo().Data.Symbols;

            using (var context = _contextFactory.Create())
            {
                foreach (var symbol in symbols)
                {
                    if (!context.StockSymbols.Any(x => x.Value == symbol.Name))
                    {
                        context.StockSymbols.Add(new StockSymbols { Value = symbol.Name });
                    }
                }

                context.SaveChanges();
            }

            _logger.LogInformation("End CheckStockSymbolsJob");
        }
    }
}