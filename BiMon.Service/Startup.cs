﻿using BiMon.Service.Jobs;
using Binance.Net;
using Binance.Net.Objects.Spot;
using BinanceMonitor.Context;
using BinanceMonitor.Library.Configs;
using BinanceMonitor.Library.Domain;
using BinanceMonitor.Library.Infrastructure;
using CryptoExchange.Net.Authentication;
using FluentScheduler;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Options;

namespace BiMon.Service
{
    public class Startup
    {
        public IConfiguration Configuration { get; }

        //public Startup(IConfiguration configuration)
        //{
        //    Configuration = configuration;
        //}

        public Startup(IHostEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                .AddEnvironmentVariables();

            Configuration = builder.Build();
        }

        public void Configure(IApplicationBuilder app, IHostEnvironment env)
        {
            JobManager.Initialize(new JobRegistry(app.ApplicationServices));
        }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddConfig(Configuration);

            services.AddSingleton<BiMonContextFactory>();
            services.AddSingleton<IUpdateService, UpdateService>();
            services.AddTransient<IEmailService, EmailService>();
            services.AddTransient<INotificationCheckService, NotificationCheckService>();

            services.AddTransient<BinanceClient>(service =>
            {
                var opt = service.GetService<IOptions<BinanceCredentialsConfig>>();
                return new BinanceClient(new BinanceClientOptions
                {
                    ApiCredentials = new ApiCredentials(opt.Value.Key, opt.Value.Secret)
                });
            });

            services.AddTransient<UpdateTradingHistoryJob>();
            services.AddTransient<CheckNotificationJob>();
            services.AddTransient<CheckStockSymbolsJob>();
        }
    }
}