﻿using BinanceMonitor.Context;
using BinanceMonitor.Library.Configs;
using Microsoft.AspNetCore.Server.Kestrel.Core;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.IO;

namespace BiMon.Service
{
    public static class ServiceCollectionExtension
    {
        public static IServiceCollection AddConfig(this IServiceCollection services, IConfiguration config)
        {
            var binanceCreds = new ConfigurationBuilder().SetBasePath(Path.Combine(AppContext.BaseDirectory))
                                                         .AddJsonFile($"Credentials/binKeys.json", optional: true, reloadOnChange: true)
                                                         .Build();

            services.Configure<BinanceCredentialsConfig>(binanceCreds.GetSection("BinanceConfig"));
            services.Configure<BiMonContextConfig>(config.GetSection("ServiceOption:MonitorContextConfig"));
            services.Configure<EmailServiceConfig>(config.GetSection("ServiceOption:EmailServiceConfig"));

            services.Configure<KestrelServerOptions>(config.GetSection("Kestrel"));

            return services;
        }
    }
}