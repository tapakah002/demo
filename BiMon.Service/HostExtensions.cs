﻿using Microsoft.AspNetCore.Hosting;
using Serilog;
using Serilog.Events;
using System;
using System.Diagnostics;
using System.IO;

namespace BiMon.Service
{
    public static class HostExtensions
    {
        public static IWebHostBuilder ConfigureSerilog(this IWebHostBuilder hostBuilder)
        {
            Serilog.Debugging.SelfLog.Enable(msg => Debug.WriteLine(msg));
            Serilog.Debugging.SelfLog.Enable(Console.Error);

            var logDir = Path.Combine(AppContext.BaseDirectory, "Logs");

            var serilogLogger = new LoggerConfiguration()
                                    .MinimumLevel.Verbose()
                                    .MinimumLevel.Override("Microsoft", LogEventLevel.Warning)
                                    .Enrich.FromLogContext()
                                    .WriteTo.Console(outputTemplate: "[{Timestamp:HH:mm:ss} {Level:u3}] {Message:lj}{NewLine}{Exception}")
                                    .WriteTo.File
                                        (
                                            path: $"{logDir}/log-.txt",
                                            rollingInterval: RollingInterval.Hour,
                                            shared: true
                                        )
                                    .CreateLogger();
            Log.Logger = serilogLogger;
            hostBuilder.UseSerilog(serilogLogger);

            return hostBuilder;
        }
    }
}