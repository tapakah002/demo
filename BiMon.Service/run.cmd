:stop
sc stop "Binance Monitor Service"

rem cause a ~10 second sleep before checking the service state
ping 127.0.0.1 -n 10 -w 1000 > nul

sc query "Binance Monitor Service" | find /I "STATE" | find "STOPPED"
if errorlevel 1 goto :stop
goto :start

:start
net start | find /i "Binance Monitor Service">nul && goto :start
sc start "Binance Monitor Service"