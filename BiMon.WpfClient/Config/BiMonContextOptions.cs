﻿using BinanceMonitor.Context;
using Microsoft.Extensions.Options;

namespace BiMon.WpfClient.Config
{
    internal class BiMonContextOptions : IOptions<BiMonContextConfig>
    {
        public BiMonContextConfig Value => _contextConfig;

        private readonly BiMonContextConfig _contextConfig;

        public BiMonContextOptions(BiMonContextConfig contextConfig)
        {
            _contextConfig = contextConfig;
        }
    }
}