﻿using AutoMapper;
using BiMon.WpfClient.Common;
using BinanceMonitor.Library.Domain.Repository;

namespace BiMon.WpfClient.Config
{
    public static class AutoMapperConfiguration
    {
        public static MapperConfiguration GetGeneralConfiguration()
        {
            return new MapperConfiguration
            (
                cfg =>
                {
                    cfg.CreateMap<Notification, NotificationRecord>()
                       .BeforeMap((x, y) => x.ItemStatus = NotificationItemStatus.Saved)
                       .ForMember(x => x.InitialPrice, x => x.MapFrom(y => y.BuyPrice))
                       .ForMember(x => x.CreateDate, x => x.MapFrom(y => y.CreateDate))
                       .ForMember(x => x.ThresholdPrice, x => x.MapFrom(y => y.SellPrice))
                       .ForMember(x => x.IsInvoked, x => x.MapFrom(y => y.IsAlarmed))
                       .ReverseMap()
                       .BeforeMap((x, y) => y.ItemStatus = NotificationItemStatus.Saved);

                    cfg.CreateMap<TradingHistoryRecord, TradingHistory>()
                       .ReverseMap();
                }
           );
        }
    }
}