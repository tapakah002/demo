﻿using BiMon.WpfClient.Common;
using BiMon.WpfClient.MVVM;
using System.Linq;
using System.Windows.Controls;
using System.Windows.Input;

namespace BiMon.WpfClient.Views
{
    public partial class Notification : UserControl
    {
        public Notification()
        {
            InitializeComponent();
        }

        private void SellCost_LostFocus(object sender, KeyboardFocusChangedEventArgs e)
        {
            TextBox tbxSell = sender as TextBox;

            var vm = DataContext as NotificationVM;
            if (e.NewFocus is Button btnCheck)
            {
                if (btnCheck.Command == vm.ApplySellPriceCommand && (int)tbxSell.Tag == (int)btnCheck.Tag)
                    return;
            }

            var sellPrice = vm.NotificationCollection.Single(x => x.Id == (int)tbxSell.Tag).SellPrice;
            tbxSell.Text = sellPrice.ToString();
        }

        private void DataGrid_CellEditEnding(object sender, DataGridCellEditEndingEventArgs e)
        {
            Common.Notification notification = (Common.Notification)e.Row.Item;
            notification.ItemStatus = NotificationItemStatus.Changed;

            System.Console.WriteLine("DataGrid_CellEditEnding");
        }

        private void DataGrid_RowEditEnding(object sender, DataGridRowEditEndingEventArgs e)
        {
            System.Console.WriteLine("DataGrid_RowEditEnding");
        }
    }
}