﻿using MVVM.BaseLibrary.Converter;
using System;
using System.Globalization;

namespace BiMon.WpfClient.Views.Converters
{
    public class HistoryItemIdConverter : MultiConverterBase<object>
    {
        public override object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            return values.Clone();
        }
    }
}