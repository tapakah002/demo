﻿namespace BiMon.WpfClient.Domain.PageViewModel
{
    public interface IPageViewModel
    {
        void OnPageChanged(object bundle);
    }
}