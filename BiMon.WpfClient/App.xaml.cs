﻿using BiMon.WpfClient.Config;
using BiMon.WpfClient.MVVM;
using BinanceMonitor.Context;
using BinanceMonitor.Library.Domain.Repository;
using BinanceMonitor.Library.Infrastructure;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Newtonsoft.Json.Linq;
using Ninject;
using Serilog;
using System.IO;
using System.Windows;

namespace BiMon.WpfClient
{
    public partial class App : Application
    {
        private IKernel _container;

        public object BinanceClient { get; private set; }

        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);

            _container = new StandardKernel();

            ConfigureConfigs();
            ConfigureLogger();
            ConfigureContainer();
            ComposeObjects();

            Current.MainWindow.Show();
        }

        private void ConfigureLogger()
        {
            Log.Logger = new LoggerConfiguration()
               .MinimumLevel.Verbose()
               .Enrich.FromLogContext()
               .Enrich.WithCaller()
               .WriteTo.Console(outputTemplate: "[{Timestamp:HH:mm:ss.fff} {Level:u3}] {Message} (at {Caller}){NewLine}{Exception}")
               .CreateLogger();
        }

        private void ConfigureContainer()
        {
            _container.Bind<IMonitorRepository>().To<MonitorRepository>().InTransientScope();
            _container.Bind<BiMonContextFactory>().ToSelf().InSingletonScope();
            _container.Bind<ILoggerFactory>().To<LoggerFactory>().InSingletonScope();
            _container.Bind<MainWindowVM>().ToSelf().InTransientScope();
        }

        private void ConfigureConfigs()
        {
            JObject appConfig;
#if DEBUG
            appConfig = JObject.Parse(File.ReadAllText(@"Config\AppConfig_Develop.json"));
#else
            appConfig = JObject.Parse(File.ReadAllText(@"Config\AppConfig_Release.json"));
#endif
            var monitorContext = appConfig["monitorContextConfig"].ToObject<BiMonContextConfig>();

            _container.Bind(typeof(IOptions<BiMonContextConfig>))
                      .To(typeof(BiMonContextOptions))
                      .WithConstructorArgument(monitorContext);
        }

        private void ComposeObjects()
        {
            Current.MainWindow = _container.Get<MainWindow>();
        }
    }
}