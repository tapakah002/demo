﻿using MVVM.BaseLibrary.NotifyBase;
using MVVM.BaseLibrary.RelayCommand;

namespace BiMon.WpfClient.MVVM
{
    public class MenuItemVM : NotifyBase
    {
        public RelayCommand ActionCommand
        {
            get => _actionCommand;
            set
            {
                _actionCommand = value;
                NotifyPropertyChanged();
            }
        }

        public string Text
        {
            get => _text;
            set
            {
                _text = value;
                NotifyPropertyChanged();
            }
        }

        private RelayCommand _actionCommand;
        private string _text;
    }
}