﻿using BiMon.WpfClient.Common;
using BiMon.WpfClient.Domain.PageViewModel;
using BiMon.WpfClient.Utils;
using BinanceMonitor.Library.Domain.Repository;
using Microsoft.Extensions.Logging;
using MVVM.BaseLibrary.NotifyBase;
using MVVM.BaseLibrary.RelayCommand;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace BiMon.WpfClient.MVVM
{
    public class MainWindowVM : NotifyBase, IPageViewModel
    {
        public IPageViewModel CurrentPageViewModel
        {
            get => _currentPageViewModel;
            set
            {
                _currentPageViewModel = value;
                NotifyPropertyChanged();
            }
        }

        public ReadOnlyObservableCollection<MenuItemVM> MenuItems { get; set; }

        private IPageViewModel _currentPageViewModel;
        private ObservableCollection<MenuItemVM> _menuItems = new ObservableCollection<MenuItemVM>();
        private Dictionary<string, IPageViewModel> _pageViewModels = new Dictionary<string, IPageViewModel>();

        public MainWindowVM(ILoggerFactory loggerFactory,
                            IMonitorRepository monitorRepository)
        {
            InitializePages(loggerFactory, monitorRepository);
            InitializeMenuItems();

            ChangePage(_pageViewModels[MenuItemTokens.Notification]);
        }

        private void ChangePage(IPageViewModel viewModel, object bundle = null)
        {
            if (CurrentPageViewModel != viewModel)
            {
                CurrentPageViewModel = viewModel;

                if (bundle != null)
                    CurrentPageViewModel.OnPageChanged(bundle);
            }
        }

        private void InitializePages(ILoggerFactory loggerFactory, IMonitorRepository monitorRepository)
        {
            _pageViewModels.Add(MenuItemTokens.TradingHistory, new TradingHistoryVM(loggerFactory, monitorRepository));
            _pageViewModels.Add(MenuItemTokens.Notification, new NotificationVM(loggerFactory, monitorRepository));
            _pageViewModels.Add(MenuItemTokens.Settings, new SettingsVM(loggerFactory, monitorRepository));

            Mediator.Subscribe(MenuItemTokens.TradingHistory, x => ChangePage(_pageViewModels[MenuItemTokens.TradingHistory], x));
            Mediator.Subscribe(MenuItemTokens.Notification, x => ChangePage(_pageViewModels[MenuItemTokens.Notification], x));
            Mediator.Subscribe(MenuItemTokens.Settings, x => ChangePage(_pageViewModels[MenuItemTokens.Settings], x));
        }

        private void InitializeMenuItems()
        {
            _menuItems.Add(new MenuItemVM { Text = "Уведомления", ActionCommand = new RelayCommand(() => Mediator.Notify(MenuItemTokens.Notification), () => true) });
            _menuItems.Add(new MenuItemVM { Text = "Настройки", ActionCommand = new RelayCommand(() => Mediator.Notify(MenuItemTokens.Settings), () => true) });
            _menuItems.Add(new MenuItemVM { Text = "История торгов", ActionCommand = new RelayCommand(() => Mediator.Notify(MenuItemTokens.TradingHistory), () => true) });

            MenuItems = new ReadOnlyObservableCollection<MenuItemVM>(_menuItems);
        }

        public void OnPageChanged(object bundle) => throw new System.NotImplementedException();
    }
}