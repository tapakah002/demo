﻿using AutoMapper;
using BiMon.WpfClient.Common;
using BiMon.WpfClient.Config;
using BiMon.WpfClient.Domain.PageViewModel;
using BiMon.WpfClient.Utils;
using BinanceMonitor.Context.Lookup;
using BinanceMonitor.Library.Domain.Repository;
using Microsoft.Extensions.Logging;
using MVVM.BaseLibrary.NotifyBase;
using MVVM.BaseLibrary.RelayCommand;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace BiMon.WpfClient.MVVM
{
    internal class TradingHistoryVM : NotifyBase, IPageViewModel
    {
        public RelayCommand UpdateCommand => _updateCommand ?? (_updateCommand = new RelayCommand(UpdateAction));
        public RelayCommand AddNewNotificationCommand => _addNewNotificationCommand ?? (_addNewNotificationCommand = new RelayCommand<TradingHistory>(AddNewNotificationAction));

        private RelayCommand _addNewNotificationCommand;
        private RelayCommand _updateCommand;
        private readonly ILoggerFactory _loggerFactory;
        private readonly IMonitorRepository _monitorRepository;

        private static readonly MapperConfiguration _mapperConfiguration = AutoMapperConfiguration.GetGeneralConfiguration();

        public ObservableCollection<TradingHistory> TradingHistoryCollection { get; set; } = new ObservableCollection<TradingHistory>();

        [Obsolete("Only for design data", true)]
        public TradingHistoryVM()
        {
        }

        public TradingHistoryVM(ILoggerFactory loggerFactory,
                                IMonitorRepository monitorRepository)
        {
            _loggerFactory = loggerFactory;
            _monitorRepository = monitorRepository;
        }

        private void UpdateAction()
        {
            var notifyList = _monitorRepository.GetTradingHistory();
            var mapper = new Mapper(_mapperConfiguration);

            List<TradingHistory> TradingHistoryItems = mapper.Map<IEnumerable<TradingHistoryRecord>, IEnumerable<TradingHistory>>(notifyList).ToList();

            foreach (var item in TradingHistoryItems)
            {
                if (TradingHistoryCollection.Count(x => x.Id == item.Id) != 0)
                    continue;

                TradingHistoryCollection.Add(item);
            }
        }

        private void AddNewNotificationAction(TradingHistory tradingHistory)
        {
            Mediator.Notify(MenuItemTokens.Notification, new Notification
            {
                ItemStatus = NotificationItemStatus.New,
                BuyPrice = tradingHistory.Price,
                NotifyType = NotifyType.Email,
                CreateDate = DateTime.Now,
                Symbol = tradingHistory.Symbol
            });
        }

        public void OnPageChanged(object bundle) => throw new System.NotImplementedException();
    }
}