﻿using BiMon.WpfClient.Domain.PageViewModel;
using BiMon.WpfClient.Utils;
using BinanceMonitor.Library.Domain.Repository;
using Microsoft.Extensions.Logging;
using MVVM.BaseLibrary.NotifyBase;
using MVVM.BaseLibrary.RelayCommand;
using System;
using System.Collections.ObjectModel;
using System.Linq;

namespace BiMon.WpfClient.MVVM
{
    public class SettingsVM : NotifyBase, IPageViewModel
    {
        public RelayCommand AddTradeSymbolCommand => _addTradeSymbolCommand ?? (_addTradeSymbolCommand = new RelayCommand<string>(AddTradeSymbolAction, CanAddTradeSymbolExecute));
        public RelayCommand RemoveTradeSymbolCommand => _removeTradeSymbolCommand ?? (_removeTradeSymbolCommand = new RelayCommand<string>(RemoveTradeSymbolAction, x => !string.IsNullOrEmpty(SelectedTradeSymbol)));

        public ReadOnlyObservableCollection<string> TradeSymbols { get; set; }
        public ObservableRangeCollection<string> StockSymbols { get; set; } = new ObservableRangeCollection<string>();
        private ObservableRangeCollection<string> _tradeSymbols { get; set; } = new ObservableRangeCollection<string>();

        public string SelectedTradeSymbol
        {
            get => _selectedTradeSymbol;
            set
            {
                _selectedTradeSymbol = value;
                NotifyPropertyChanged();
            }
        }

        public string SelectedStockSymbol
        {
            get => _selectedStockSymbol;
            set
            {
                _selectedStockSymbol = value;
                NotifyPropertyChanged();
            }
        }

        public string StockSymbolFilter
        {
            get => _stockSymbolFilter;
            set
            {
                _stockSymbolFilter = value;
                NotifyPropertyChanged();
                FilterStockSymbols();
            }
        }

        private RelayCommand _addTradeSymbolCommand;
        private RelayCommand _removeTradeSymbolCommand;
        private readonly ILoggerFactory _loggerFactory;
        private readonly IMonitorRepository _monitorRepository;
        private string _selectedTradeSymbol;
        private string _selectedStockSymbol;
        private string _stockSymbolFilter;

        [Obsolete("Only for design data", true)]
        public SettingsVM()
        {
        }

        public SettingsVM(ILoggerFactory loggerFactory,
                          IMonitorRepository monitorRepository)
        {
            _loggerFactory = loggerFactory;
            _monitorRepository = monitorRepository;

            _tradeSymbols.AddRange(_monitorRepository.GetTradeSymbols());
            TradeSymbols = new ReadOnlyObservableCollection<string>(_tradeSymbols);
            StockSymbols.AddRange(_monitorRepository.GetStockSymbols());
        }

        private void AddTradeSymbolAction(string symbols)
        {
            _monitorRepository.AddTradeSymbols(symbols);

            _tradeSymbols.Clear();
            _tradeSymbols.AddRange(_monitorRepository.GetTradeSymbols());
        }

        private void RemoveTradeSymbolAction(string symbols)
        {
            _monitorRepository.RemoveTradeSymbols(symbols);

            _tradeSymbols.Clear();
            _tradeSymbols.AddRange(_monitorRepository.GetTradeSymbols());

            SelectedTradeSymbol = null;
        }

        private void FilterStockSymbols()
        {
            StockSymbols.Clear();
            if (string.IsNullOrEmpty(StockSymbolFilter))
                StockSymbols.AddRange(_monitorRepository.GetStockSymbols());
            else
            {
                var allSymbols = _monitorRepository.GetStockSymbols();

                StockSymbols.AddRange(allSymbols.Where(x => x.ToUpper().Contains(StockSymbolFilter.ToUpper())));
            }
        }

        private bool CanAddTradeSymbolExecute(string arg) => !string.IsNullOrEmpty(arg);
        public void OnPageChanged(object bundle) => throw new System.NotImplementedException();
    }
}