﻿using AutoMapper;
using BiMon.WpfClient.Common;
using BiMon.WpfClient.Config;
using BiMon.WpfClient.Domain.PageViewModel;
using BiMon.WpfClient.Utils;
using BinanceMonitor.Library.Domain.Repository;
using Microsoft.Extensions.Logging;
using MVVM.BaseLibrary.NotifyBase;
using MVVM.BaseLibrary.RelayCommand;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;

namespace BiMon.WpfClient.MVVM
{
    internal class NotificationVM : NotifyBase, IPageViewModel
    {
        public RelayCommand UpdateCommand => _updateCommand ?? (_updateCommand = new RelayCommand(UpdateAction));
        public RelayCommand AddNewNotificationCommand => _addNewNotificationCommand ?? (_addNewNotificationCommand = new RelayCommand(AddNewNotificationAction));
        public RelayCommand SaveNotificationCommand => _saveNotificationCommand ?? (_saveNotificationCommand = new RelayCommand<Notification>(SaveNotificationAction));
        public RelayCommand RemoveNotificationCommand => _removeNotificationCommand ?? (_removeNotificationCommand = new RelayCommand<Notification>(RemoveNotificationAction));
        public RelayCommand ApplySellPriceCommand => _applySellPriceCommand ?? (_applySellPriceCommand = new RelayCommand<object>(ApplySellPriceAction));

        private RelayCommand _addNewNotificationCommand;
        private RelayCommand _updateCommand;
        private RelayCommand _applySellPriceCommand;
        private RelayCommand _saveNotificationCommand;
        private RelayCommand _removeNotificationCommand;
        private readonly ILoggerFactory _loggerFactory;
        private readonly IMonitorRepository _monitorRepository;

        private static readonly MapperConfiguration _mapperConfiguration = AutoMapperConfiguration.GetGeneralConfiguration();

        public ObservableCollection<Notification> NotificationCollection { get; set; } = new ObservableCollection<Notification>();
        public ObservableCollection<string> AvaliableSymbols => GetAvaliableSymbols();

        private ObservableRangeCollection<string> _avaliableSymbols = new ObservableRangeCollection<string>();

        [Obsolete("Only for design data", true)]
        public NotificationVM()
        {
        }

        public NotificationVM(ILoggerFactory loggerFactory,
                              IMonitorRepository monitorRepository)
        {
            _loggerFactory = loggerFactory;
            _monitorRepository = monitorRepository;
        }

        private void AddNewNotificationAction()
        {
            if (!NotificationCollection.Any(x => x.ItemStatus == NotificationItemStatus.New))
                NotificationCollection.Add(new Notification { ItemStatus = NotificationItemStatus.New, CreateDate = DateTime.Now });
        }

        private void UpdateAction()
        {
            UpdateAvaliableSymbols();

            var notifyList = _monitorRepository.GetNotifications();
            var mapper = new Mapper(_mapperConfiguration);

            List<Notification> NotificationItems = mapper.Map<IEnumerable<NotificationRecord>, IEnumerable<Notification>>(notifyList).ToList();

            foreach (var item in NotificationItems)
            {
                if (NotificationCollection.Count(x => x.Id == item.Id) == 0)
                    NotificationCollection.Add(item);
            }
        }

        private void ApplySellPriceAction(object id)
        {
            var data = id as object[];

            var item = NotificationCollection.Single(x => x.Id == (int)data[0]);
            item.SellPrice = double.Parse(((string)data[1]).Replace(',', '.'), CultureInfo.InvariantCulture);
            item.ItemStatus = NotificationItemStatus.Changed;
        }

        private void SaveNotificationAction(Notification notification)
        {
            var mapper = new Mapper(_mapperConfiguration);
            var record = mapper.Map<Notification, NotificationRecord>(notification);

            _monitorRepository.SaveNotification(record);

            var updatedNoty = NotificationCollection.Single(x => x.Id == notification.Id);
            notification.ItemStatus = NotificationItemStatus.Saved;
            notification.Id = record.Id;
        }

        private void RemoveNotificationAction(Notification notification)
        {
            var mapper = new Mapper(_mapperConfiguration);
            _monitorRepository.RemoveNotification(mapper.Map<Notification, NotificationRecord>(notification));
            NotificationCollection.Remove(NotificationCollection.Single(x => x.Id == notification.Id));
        }

        private ObservableCollection<string> GetAvaliableSymbols()
        {
            UpdateAvaliableSymbols();
            return _avaliableSymbols;
        }

        private void UpdateAvaliableSymbols()
        {
            foreach (var item in _monitorRepository?.GetTradeSymbols())
            {
                if (!_avaliableSymbols.Contains(item))
                    _avaliableSymbols.Add(item);
            }
        }

        public void OnPageChanged(object bundle)
        {
            if (!(bundle is Notification notification))
            {
                throw new Exception(nameof(bundle));
            }
            else
            {
                notification.ItemStatus = NotificationItemStatus.New;
                if (!NotificationCollection.Any(x => x.ItemStatus == NotificationItemStatus.New))
                {
                    NotificationCollection.Add(notification);
                }
                else
                {
                    notification.ItemStatus = NotificationItemStatus.New;

                    var existingItem = NotificationCollection.Single(x => x.ItemStatus == NotificationItemStatus.New);
                    var indexOfExistingItem = NotificationCollection.IndexOf(existingItem);
                    NotificationCollection.Insert(indexOfExistingItem, notification);
                    NotificationCollection.Remove(existingItem);
                }
            }
        }
    }
}