﻿namespace BiMon.WpfClient.Common
{
    public enum NotificationItemStatus
    {
        New,
        Saved,
        Changed
    }
}