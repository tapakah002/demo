﻿namespace BiMon.WpfClient.Common
{
    public static class MenuItemTokens
    {
        public static string TradingHistory { get => "GoToTradingHistory"; }
        public static string Notification { get => "GoToNotification"; }
        public static string Settings { get => "GoToSettings"; }
    }
}