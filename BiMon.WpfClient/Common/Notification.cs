﻿using BinanceMonitor.Context.Lookup;
using MVVM.BaseLibrary.NotifyBase;
using System;

namespace BiMon.WpfClient.Common
{
    public class Notification : NotifyBase
    {
        public double BuyPrice
        {
            get => buyPrice;
            set
            {
                if (value != this.buyPrice)
                {
                    this.buyPrice = value;
                    NotifyPropertyChanged();
                }
            }
        }

        public DateTime CreateDate
        {
            get => createDate; set
            {
                if (value != this.createDate)
                {
                    this.createDate = value;
                    NotifyPropertyChanged();
                }
            }
        }

        public int Id
        {
            get => id; set
            {
                if (value != this.id)
                {
                    this.id = value;
                    NotifyPropertyChanged();
                }
            }
        }

        public bool IsAlarmed
        {
            get => isAlarmed; set
            {
                if (value != this.isAlarmed)
                {
                    this.isAlarmed = value;
                    NotifyPropertyChanged();
                }
            }
        }

        public NotificationItemStatus ItemStatus
        {
            get => itemStatus; set
            {
                if (value != this.itemStatus)
                {
                    this.itemStatus = value;
                    NotifyPropertyChanged();
                }
            }
        }

        public NotifyType NotifyType
        {
            get => notifyType; set
            {
                if (value != this.notifyType)
                {
                    this.notifyType = value;
                    NotifyPropertyChanged();
                }
            }
        }

        public double SellPrice
        {
            get => sellPrice; set
            {
                if (value != this.sellPrice)
                {
                    this.sellPrice = value;
                    NotifyPropertyChanged();
                }
            }
        }

        public string Symbol
        {
            get => symbol; set
            {
                if (value != this.symbol)
                {
                    this.symbol = value;
                    NotifyPropertyChanged();
                }
            }
        }

        private double buyPrice;
        private DateTime createDate;
        private int id;
        private bool isAlarmed;
        private NotificationItemStatus itemStatus;
        private NotifyType notifyType;
        private double sellPrice;
        private string symbol;
    }
}