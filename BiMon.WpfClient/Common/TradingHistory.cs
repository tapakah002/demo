﻿using BinanceMonitor.Context.Lookup;
using MVVM.BaseLibrary.NotifyBase;
using System;

namespace BiMon.WpfClient.Common
{
    public class TradingHistory : NotifyBase
    {
        public double Price
        {
            get => buyPrice;
            set
            {
                if (value != this.buyPrice)
                {
                    this.buyPrice = value;
                    NotifyPropertyChanged();
                }
            }
        }

        public DateTime CreateDate
        {
            get => createDate; set
            {
                if (value != this.createDate)
                {
                    this.createDate = value;
                    NotifyPropertyChanged();
                }
            }
        }

        public int Id
        {
            get => id; set
            {
                if (value != this.id)
                {
                    this.id = value;
                    NotifyPropertyChanged();
                }
            }
        }

        public string Symbol
        {
            get => symbol;
            set
            {
                if (value != this.symbol)
                {
                    this.symbol = value;
                    NotifyPropertyChanged();
                }
            }
        }

        public TradeType TradeType
        {
            get => type;
            set
            {
                if (value != this.type)
                {
                    this.type = value;
                    NotifyPropertyChanged();
                }
            }
        }

        public double Quantity
        {
            get => quantity;

            set
            {
                quantity = value;
                NotifyPropertyChanged();
            }
        }

        private double buyPrice;
        private DateTime createDate;
        private int id;
        private string symbol;
        private TradeType type;
        private double quantity;
    }
}