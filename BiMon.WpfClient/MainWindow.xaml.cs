﻿using BiMon.WpfClient.MVVM;
using System.Windows;

namespace BiMon.WpfClient
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow(MainWindowVM vm)
        {
            DataContext = vm;
            InitializeComponent();
        }

        private void Window_Closing(object sender,
             System.ComponentModel.CancelEventArgs e)
        {
            Properties.Settings.Default.Save();
        }
    }
}