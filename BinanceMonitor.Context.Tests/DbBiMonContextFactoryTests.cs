using BinanceMonitor.Context.Entities;
using Microsoft.Extensions.Logging.Abstractions;
using Microsoft.Extensions.Options;
using Moq;
using System;
using Xunit;

namespace BinanceMonitor.Context.Tests
{
    public class DbBiMonContextFactoryTests
    {
        private MockRepository mockRepository;

        private Mock<IOptions<BiMonContextConfig>> mockOptions;

        public DbBiMonContextFactoryTests()
        {
            this.mockRepository = new MockRepository(MockBehavior.Strict);

            this.mockOptions = this.mockRepository.Create<IOptions<BiMonContextConfig>>();
            mockOptions.Setup(x => x.Value).Returns(new BiMonContextConfig { ConnectionString = @"Data Source=(localdb)\MSSQLLocalDB;Initial Catalog=BinanceMonitor;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False" });
        }

        private BiMonContextFactory CreateFactory()
        {
            return new BiMonContextFactory(
                this.mockOptions.Object,
                NullLoggerFactory.Instance);
        }

        [Fact]
        public void Create_StateUnderTest_ExpectedBehavior()
        {
            // Arrange
            var factory = this.CreateFactory();
            bool useLogger = false;

            // Act
            var result = factory.Create(
                useLogger);

            result.Notifications.Add(new Notifications { InitialPrice = 1, Comment = "asd", CreateDate = DateTime.Now, Symbol = "VTC" });
            result.SaveChanges();

            // Assert
            Assert.True(false);
            this.mockRepository.VerifyAll();
        }
    }
}