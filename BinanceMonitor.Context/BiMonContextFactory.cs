﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace BinanceMonitor.Context
{
    public class BiMonContextFactory
    {
        private readonly BiMonContextConfig _options;
        private readonly ILoggerFactory _loggerFactory;
        private readonly ILogger<BiMonContextFactory> _logger;

        private object _locker = new object();
        private static bool _isMigrated = false;

        public BiMonContextFactory(IOptions<BiMonContextConfig> options,
                                   ILoggerFactory loggerFactory)
        {
            _options = options.Value;
            _loggerFactory = loggerFactory;
            _logger = _loggerFactory.CreateLogger<BiMonContextFactory>();
        }

        public BiMonContext Create(bool useLogger = false)
        {
            var optionBuilder = new DbContextOptionsBuilder<BiMonContext>()
                               .UseNpgsql(_options.ConnectionString)
                               .EnableSensitiveDataLogging();

            BiMonContext context;

            if (useLogger)
            {
                if (_loggerFactory == null)
                    _logger.LogWarning("Logger factory is null. Context will be created without logger");
                context = new BiMonContext(optionBuilder.Options, _loggerFactory);
            }
            else
                context = new BiMonContext(optionBuilder.Options);

            if (!_isMigrated)
            {
                lock (_locker)
                {
                    if (!_isMigrated)
                    {
                        context.Database.Migrate();
                        _isMigrated = true;
                    }
                }
            }

            return context;
        }
    }
}