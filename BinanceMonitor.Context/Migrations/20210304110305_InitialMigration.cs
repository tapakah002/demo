﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace BinanceMonitor.Context.Migrations
{
    public partial class InitialMigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "NotifyTypeLookup",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_NotifyTypeLookup", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "StockSymbols",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Value = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_StockSymbols", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "TradeSymbols",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:IdentitySequenceOptions", "'3', '1', '', '', 'False', '1'")
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Value = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TradeSymbols", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "TradeTypeLookup",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TradeTypeLookup", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Notifications",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Symbol = table.Column<string>(nullable: false),
                    InitialPrice = table.Column<double>(nullable: true),
                    ThresholdPrice = table.Column<double>(nullable: true),
                    CreateDate = table.Column<DateTime>(nullable: false),
                    IsInvoked = table.Column<bool>(nullable: false),
                    Comment = table.Column<string>(nullable: true),
                    NotifyType = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Notifications", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Notifications_NotifyTypeLookup_NotifyType",
                        column: x => x.NotifyType,
                        principalTable: "NotifyTypeLookup",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "TradingHistory",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    CreateDate = table.Column<DateTime>(nullable: false),
                    OrderId = table.Column<long>(nullable: false),
                    Price = table.Column<double>(nullable: false),
                    Quantity = table.Column<double>(nullable: false),
                    Symbol = table.Column<string>(nullable: false),
                    Commission = table.Column<double>(nullable: false),
                    TradeType = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TradingHistory", x => x.Id);
                    table.ForeignKey(
                        name: "FK_TradingHistory_TradeTypeLookup_TradeType",
                        column: x => x.TradeType,
                        principalTable: "TradeTypeLookup",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "NotifyTypeLookup",
                columns: new[] { "Id", "Name" },
                values: new object[,]
                {
                    { 0, "Email" },
                    { 1, "Sms" }
                });

            migrationBuilder.InsertData(
                table: "TradeSymbols",
                columns: new[] { "Id", "Value" },
                values: new object[,]
                {
                    { 1, "BTCUSDT" },
                    { 2, "ETHUSDT" }
                });

            migrationBuilder.InsertData(
                table: "TradeTypeLookup",
                columns: new[] { "Id", "Name" },
                values: new object[,]
                {
                    { 0, "Продажа" },
                    { 1, "Покупка" }
                });

            migrationBuilder.CreateIndex(
                name: "IX_Notifications_NotifyType",
                table: "Notifications",
                column: "NotifyType");

            migrationBuilder.CreateIndex(
                name: "IX_TradingHistory_TradeType",
                table: "TradingHistory",
                column: "TradeType");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Notifications");

            migrationBuilder.DropTable(
                name: "StockSymbols");

            migrationBuilder.DropTable(
                name: "TradeSymbols");

            migrationBuilder.DropTable(
                name: "TradingHistory");

            migrationBuilder.DropTable(
                name: "NotifyTypeLookup");

            migrationBuilder.DropTable(
                name: "TradeTypeLookup");
        }
    }
}
