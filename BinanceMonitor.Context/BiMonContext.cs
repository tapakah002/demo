﻿using BinanceMonitor.Context.Entities;
using BinanceMonitor.Context.Lookup;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;

namespace BinanceMonitor.Context
{
    public class BiMonContext : DbContext, IBiMonContext
    {
        public DbSet<Notifications> Notifications { get; set; }
        public DbSet<NotifyTypeLookup> NotifyTypeLookup { get; set; }
        public DbSet<TradeSymbols> TradeSymbols { get; set; }
        public DbSet<StockSymbols> StockSymbols { get; set; }
        public DbSet<TradeTypeLookup> TradeTypeLookup { get; set; }
        public DbSet<TradingHistory> TradingHistory { get; set; }

        private readonly ILoggerFactory _loggerFactory;

        public BiMonContext(DbContextOptions options, ILoggerFactory factory = null) : base(options)
        {
            _loggerFactory = factory;
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseLoggerFactory(_loggerFactory);

            base.OnConfiguring(optionsBuilder);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            InitializeNotifyLookup(modelBuilder);
            InitializeTradeTypeLookup(modelBuilder);
            InitializeSymbols(modelBuilder);
        }

        private void InitializeNotifyLookup(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<NotifyTypeLookup>()
                .Property(s => s.Id)
                .HasConversion<int>();

            modelBuilder.Entity<NotifyTypeLookup>()
                   .HasData
                   (
                        Enum.GetValues(typeof(NotifyType))
                            .Cast<NotifyType>()
                            .Select(e => new NotifyTypeLookup()
                            {
                                Id = e,
                                Name = e.GetType()
                                        .GetMember(e.ToString())
                                        .First()
                                        .GetCustomAttribute<DisplayAttribute>()
                                        .GetName()
                            })
                   );
        }

        private void InitializeSymbols(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<TradeSymbols>().HasData
                (
                    new TradeSymbols { Id = 1, Value = "BTCUSDT" },
                    new TradeSymbols { Id = 2, Value = "ETHUSDT" }
                );
            modelBuilder.Entity<TradeSymbols>().Property(x => x.Id).HasIdentityOptions(startValue: 2 + 1);

        }

        private void InitializeTradeTypeLookup(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<TradeTypeLookup>()
                .Property(s => s.Id)
                .HasConversion<int>();

            modelBuilder.Entity<TradeTypeLookup>()
                   .HasData
                   (
                        Enum.GetValues(typeof(TradeType))
                            .Cast<TradeType>()
                            .Select(e => new TradeTypeLookup()
                            {
                                Id = e,
                                Name = e.GetType()
                                        .GetMember(e.ToString())
                                        .First()
                                        .GetCustomAttribute<DisplayAttribute>()
                                        .GetName()
                            })
                   );
        }
    }
}