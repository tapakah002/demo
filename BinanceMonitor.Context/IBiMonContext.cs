﻿using BinanceMonitor.Context.Entities;
using Microsoft.EntityFrameworkCore;

namespace BinanceMonitor.Context
{
    public interface IBiMonContext
    {
        DbSet<NotifyTypeLookup> NotifyTypeLookup { get; set; }
        DbSet<Notifications> Notifications { get; set; }
    }
}