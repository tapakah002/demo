﻿using System.ComponentModel.DataAnnotations;

namespace BinanceMonitor.Context.Entities
{
    public class TradeSymbols
    {
        public int Id { get; set; }

        [Required]
        public string Value { get; set; }
    }
}