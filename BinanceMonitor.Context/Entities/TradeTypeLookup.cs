﻿using BinanceMonitor.Context.Lookup;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace BinanceMonitor.Context.Entities
{
    public class TradeTypeLookup
    {
        public TradeType Id { get; set; }

        [Required]
        public string Name { get; set; }

        public virtual List<TradingHistory> TradingHistory { get; set; }
    }
}