﻿using System;

namespace BinanceMonitor.Context.Entities
{
    public class NotificationLogs
    {
        public int Id { get; set; }

        public DateTime CreateDate { get; set; }

        public bool IsSuccess { get; set; }

        public int NotificationId { get; set; }
        public virtual Notifications Notification { get; set; }
    }
}