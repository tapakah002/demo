﻿using BinanceMonitor.Context.Lookup;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BinanceMonitor.Context.Entities
{
    public class Notifications
    {
        public int Id { get; set; }

        [Required]
        public string Symbol { get; set; }

        public double? InitialPrice { get; set; }
        public double? ThresholdPrice { get; set; }
        public DateTime CreateDate { get; set; }
        public bool IsInvoked { get; set; }
        public string Comment { get; set; }
        public NotifyType NotifyType { get; set; }

        [ForeignKey("NotifyType")]
        public virtual NotifyTypeLookup NotifyTypeLookup { get; set; }
    }
}