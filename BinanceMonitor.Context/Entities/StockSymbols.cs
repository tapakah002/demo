﻿namespace BinanceMonitor.Context.Entities
{
    public class StockSymbols
    {
        public int Id { get; set; }
        public string Value { get; set; }
    }
}