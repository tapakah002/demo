﻿using BinanceMonitor.Context.Lookup;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Diagnostics;

namespace BinanceMonitor.Context.Entities
{
    [DebuggerDisplay("Id = {Id}; Symbol = {Symbol}; Price = {Price}; Quantity = {Quantity}")]
    public class TradingHistory
    {
        public int Id { get; set; }

        public DateTime CreateDate { get; set; }

        public long OrderId { get; set; }

        public double Price { get; set; }
        public double Quantity { get; set; }

        [Required]
        public string Symbol { get; set; }

        public double Commission { get; set; }
        public TradeType TradeType { get; set; }

        [ForeignKey("TradeType")]
        public virtual TradeTypeLookup TradeTypeLookup { get; set; }
    }
}