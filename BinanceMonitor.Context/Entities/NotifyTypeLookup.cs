﻿using BinanceMonitor.Context.Lookup;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace BinanceMonitor.Context.Entities
{
    public class NotifyTypeLookup
    {
        public NotifyType Id { get; set; }

        [Required]
        public string Name { get; set; }

        public virtual List<Notifications> Notifications { get; set; }
    }
}