﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using System;

namespace BinanceMonitor.Context.MigrationStubs
{
    public class BiMonContextFactoryStub : IDesignTimeDbContextFactory<BiMonContext>
    {
        public BiMonContext CreateDbContext(string[] args)
        {
            var optionsBuilder = new DbContextOptionsBuilder<BiMonContext>();

            optionsBuilder.UseNpgsql(@"Host=localhost;Username=bimon_user;Password=a1s2d3f4;Database=BinanceMonitor_Dev", opts => opts.CommandTimeout((int)TimeSpan.FromMinutes(10).TotalSeconds));
            return new BiMonContext(optionsBuilder.Options);
        }
    }
}