﻿using System.ComponentModel.DataAnnotations;

namespace BinanceMonitor.Context.Lookup
{
    public enum TradeType
    {
        [Display(Name = "Продажа")]
        Sell,

        [Display(Name = "Покупка")]
        Buy
    }
}