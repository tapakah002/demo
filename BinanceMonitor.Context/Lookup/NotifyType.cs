﻿using System.ComponentModel.DataAnnotations;

namespace BinanceMonitor.Context.Lookup
{
    public enum NotifyType
    {
        [Display(Name = "Email")]
        Email,

        [Display(Name = "Sms")]
        Sms
    }
}