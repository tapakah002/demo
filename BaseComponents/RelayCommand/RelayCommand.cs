﻿using System;
using System.Diagnostics;
using System.Windows.Input;

namespace MVVM.BaseLibrary.RelayCommand
{
    public class RelayCommand : ICommand
    {
        public event EventHandler CanExecuteChanged
        {
            add => CommandManager.RequerySuggested += value;
            remove => CommandManager.RequerySuggested -= value;
        }

        private readonly Action _executeAction;
        private readonly Func<bool> _canExecuteFunction;

        public RelayCommand()
        { }

        public RelayCommand(Action execute) : this(execute, null)
        { }

        public RelayCommand(Action execute, Func<bool> canExecute)
        {
            _executeAction = execute ?? throw new ArgumentException(nameof(execute));
            _canExecuteFunction = canExecute;
        }

        [DebuggerStepThrough]
        public virtual bool CanExecute(object parameter)
        {
            return _canExecuteFunction == null || _canExecuteFunction();
        }

        public virtual void Execute(object parameter)
        {
            _executeAction();
        }
    }
}