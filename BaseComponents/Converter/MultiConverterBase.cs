﻿using System;
using System.Globalization;
using System.Windows.Data;
using System.Windows.Markup;

namespace MVVM.BaseLibrary.Converter
{
    public abstract class MultiConverterBase<T> : MarkupExtension, IMultiValueConverter

    where T : class, new()
    {
        public abstract object Convert(object[] values, Type targetType, object parameter, CultureInfo culture);

        public virtual object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            return new object[] { this };
        }

        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            return this;
        }
    }
}