﻿using System;
using System.Globalization;
using System.Windows.Data;
using System.Windows.Markup;

namespace MVVM.BaseLibrary.Converter
{
    public abstract class ConverterBase<T> : MarkupExtension, IValueConverter
where T : class, new()
    {
        public abstract object Convert(object value, Type targetType, object parameter, CultureInfo culture);

        public virtual object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return value;
        }

        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            return this;
        }
    }
}